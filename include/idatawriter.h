#ifndef MY_WGET_APP_IDATA_WRITER_H
#define MY_WGET_APP_IDATA_WRITER_H

#include <string>
namespace my_wget_app{
    class IDataWriter{
        public:
        virtual ~IDataWriter() = default;
        virtual int write(const char* data, int size) = 0;
        virtual int write(const std::string& data) = 0;
    };
}

#endif