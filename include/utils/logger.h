#ifndef MY_COM_LOGGER_H
#define MY_COM_LOGGER_H

#include <ostream>
#include <memory>

//************
// log system
//************

namespace my_wget_app::utils{
    class ILoger{
        public:
        using StreamType = std::basic_ostream<char>;
        virtual StreamType& getStream() const = 0;
        virtual ~ILoger() = default;
    };
    
    template <typename T>
    ILoger& operator<<(ILoger& logger, const T& value){
        logger.getStream()<<value;
        return logger;
    }
    
    class Logger{
        public:
        static ILoger& log();
        private:
        Logger();
        private:
        std::shared_ptr<ILoger> m_pimpl;
    };
}




#endif