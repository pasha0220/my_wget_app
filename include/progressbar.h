#ifndef MY_COM_PROGRESSBAR_H
#define MY_COM_PROGRESSBAR_H

namespace my_wget_app::utils{

    class Progressbar {
    public:
        explicit  Progressbar(int stepCount);
        void makeStep();
    private:
        void print() const;

    private:
        int mStepCount;
        float mDonePercent;
        float mStepWeight;
    };
}




#endif //MY_COM_PROGRESSBAR_H
