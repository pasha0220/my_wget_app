#ifndef MY_WGET_APP_HEADERS_H
#define MY_WGET_APP_HEADERS_H

#include <string>
#include <iterator>
#include <string_view>
#include <unordered_map>

namespace my_wget_app{
    
    class Headers{
        public:
        using HeaderName = std::string;
        using HeaderValue = std::string;
        using HeadersMap = std::unordered_map<HeaderName, HeaderValue>;
//    public:
//        struct Header{
//            Header(const HeadersMap::iterator& it): mIt(it){}
//
//            std::string_view name() const;
//            std::string_view value() const;
//
//            HeadersMap::iterator mIt;
//        };
//
//        class HeaderIterator: public std::iterator<std::input_iterator_tag, Header> {
//            explicit HeaderIterator(Header&& h) : h_ref(h) {}
//            iterator& operator++() { std::next(h_ref.mIt); return *this;}
//            iterator operator++(int) {iterator retval = *this; ++(*this); return retval;}
//            bool operator==(const iterator& other) const {return h_ref.mIt == h_ref.mIt;}
//            bool operator!=(const iterator& other) const {return !(*this == other);}
//            reference operator*() const {return h_ref;}
//        private:
//            Header h_ref;
//        };

    public:
        Headers();
        using ConstHeaderIterator = HeadersMap::const_iterator;
        using HeaderIterator = HeadersMap::iterator;
        ConstHeaderIterator begin() const;
        ConstHeaderIterator end() const;
        HeaderIterator begin();
        HeaderIterator end();

        void add(const HeaderName& name, const HeaderValue& value);
        const HeaderValue& get(const HeaderName& name) const;
        private:
        HeadersMap m_headers;
    };
}

#endif