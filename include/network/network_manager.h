#ifndef MY_WGET_APP_NETWORK_MANAGER_H
#define MY_WGET_APP_NETWORK_MANAGER_H

#include <memory>

namespace my_wget_app{
    class Request;
    class Response;

    class NetworkManager{
        public:
        static std::unique_ptr<Response> get(const Request& );
        static std::unique_ptr<Response> post(const Request&);
    };
}

#endif