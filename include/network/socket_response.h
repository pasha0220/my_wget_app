#ifndef MY_WGET_APP_SOCKET_RESPONSE_H
#define MY_WGET_APP_SOCKET_RESPONSE_H

#include "response.h"
#include <array>
#include <string_view>

namespace my_wget_app{
    class SocketResponse: public Response {
    public:
        using SocketDescription = int;
        SocketResponse(SocketDescription sock);
        ~SocketResponse();

    public:
        virtual int statusCode() const override;
        virtual std::string_view reasonPhrase() const override;
        virtual size_t contentLength() const override;

        virtual const Headers& headers() const override;

        virtual int readContent(DataBuf& dataChunk) override;
    private:
        void init();
        void parseStatusLine(std::string_view str);
        void parseHeaderLine(std::string_view str);
        void readSome(Response::DataBuf& buf, size_t bytesToRead) const;
    private:
        SocketDescription m_sock;
        int mStatusCode;
        size_t mDataToRead;
        size_t mContentLength;
        std::string mReasonPhrase;
        Headers mResponseHeaders;
        DataBuf mRawData;
    };
}

#endif