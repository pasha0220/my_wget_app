#ifndef MY_WGET_APP_SOCKET_DATA_WRITER_H
#define MY_WGET_APP_SOCKET_DATA_WRITER_H

#include "idatawriter.h"

namespace my_wget_app{
    class SocketDataWriter: public IDataWriter{
        public:
        SocketDataWriter(int socket_desct);
        virtual int write(const char* data, int size) override;
        virtual int write(const std::string& data) override;
        private:
        int mSocket;
    };
}


#endif