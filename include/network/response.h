#ifndef MY_WGET_APP_RESPONSE_H
#define MY_WGET_APP_RESPONSE_H

#include "headers.h"

#include <cstdint>
#include <vector>
#include <string_view>

namespace my_wget_app{
    class Response{
    public:
        using DataBuf = std::vector<std::uint8_t>;
    public:
        virtual ~Response() = default;

        virtual int statusCode() const = 0;
        virtual std::string_view reasonPhrase() const = 0;
        virtual size_t contentLength() const = 0;

        virtual const Headers& headers() const = 0;

        virtual int readContent(DataBuf& dataChunk) = 0;
    };
}

#endif