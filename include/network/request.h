#ifndef MY_WGET_APP_REQUEST_H
#define MY_WGET_APP_REQUEST_H

#include "network/uri.h"
#include "network/headers.h"

//*****************
//network request
//*****************

namespace my_wget_app{

    class Request{
        public:
        Request(const Uri& uri, const Headers& header /*, cookies*/ );
        // check if request is valid
        explicit operator bool() const;
        
        const Uri& uri() const;
        void setUri(const Uri& uri);

        const Headers& headers() const;
        Headers& headers();
        
        private:
        Uri m_uri;
        Headers m_headers;

    };
}

#endif