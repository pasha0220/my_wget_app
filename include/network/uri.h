#ifndef MY_WGET_APP_URI_H
#define MY_WGET_APP_URI_H

#include <string>
#include <string_view>
#include <optional>

namespace my_wget_app{
    class Uri{
    public:
        explicit Uri(const std::string& uri);
        Uri(const std::string& host, const std::string& path);
        Uri(const Uri&);
        Uri(Uri&&);

        Uri& operator=(const Uri&);
        Uri& operator=(Uri&&);
        
        explicit operator bool() const;
        explicit operator const std::string&() const;

        std::string_view hostname() const;
        int port() const;
        std::string_view path() const;
    private:
        std::string mUri;
        std::optional<int> mPort;
        std::string mHost;
        std::string mPath;
    };
}
#endif