#ifndef MY_COM_DATAQUEUE_H
#define MY_COM_DATAQUEUE_H

#include <atomic>
#include <condition_variable>
#include <queue>
#include <mutex>

namespace my_wget_app::utils{
    template <typename T>
    class Dataqueue {
    public:
        void push(T&& v){
            std::unique_lock<std::mutex> lk(mMutex);
            mQueue.emplace(v);
            mDataCv.notify_one();
        }
        bool pop(T& v){
            std::unique_lock<std::mutex> lk(mMutex);
            mDataCv.wait(lk, [this](){ return mIsDone || !mQueue.empty(); });
            if (mQueue.empty()){
                return false;
            }
            v =  mQueue.front();
            mQueue.pop();
            return true;
        }

        bool isDone() const{
            if (!mIsDone){
                return false;
            }
            std::unique_lock<std::mutex> lk(mMutex);
            return mQueue.empty();

        }
        void setDone() {
            mIsDone = true;
            mDataCv.notify_one();
        }

    private:
        mutable std::mutex mMutex;
        std::atomic<bool> mIsDone = false;
        std::condition_variable mDataCv;
        std::queue<T> mQueue;
    };
}




#endif //MY_COM_DATAQUEUE_H
