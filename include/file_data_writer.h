#ifndef MY_COM_FILE_DATA_WRITER_H
#define MY_COM_FILE_DATA_WRITER_H

#include "idatawriter.h"

#include <fstream>

namespace my_wget_app::utils{
    class FileDataWriter : public my_wget_app::IDataWriter{
    public:
        explicit FileDataWriter(const std::string& path);
        virtual int write(const char* data, int size) override;
        virtual int write(const std::string& data) override;
    private:
        std::ofstream mFstream;
    };
}


#endif //MY_COM_FILE_DATA_WRITER_H
