#include <iostream>

#include "dataqueue.h"

#include "network/request.h"
#include "network/response.h"
#include "network/network_manager.h"
#include "utils/logger.h"
#include "file_data_writer.h"
#include "progressbar.h"

#include <set>
#include <unordered_set>
#include <thread>
#include <cmath>

using namespace my_wget_app;
using namespace my_wget_app::utils;

namespace {

    enum ArgNums{
        URI_ARG = 1
        , LOCAL_FILE_PATH = 2
        , LAST = 2
    };

    static const auto sDefaultUri = "localhost";
    static const auto sDefaultFilePath = "./my_wget_app_result.txt";


    template <typename T>
    struct ProgressMixin: public T {
        template <typename ...Args>
        ProgressMixin(int stepCount, Args&&... args)
                : T(std::forward<Args>(args)...)
                , mProgress(stepCount)
        {}

        virtual int write(const char* data, int size) override {
            auto res = T::write(data, size);
            mProgress.makeStep();
            return res;
        }
        virtual int write(const std::string& data) override {
            auto res = T::write(data);
            mProgress.makeStep();
            return res;
        }

    private:
        Progressbar mProgress;
    };


    std::unique_ptr<my_wget_app::Response> sendRequest(const std::string& url){
        auto isRedirResponse = [](auto code){
            static const std::set<int> suportedRedirCodes{301, 307, 308};
            return suportedRedirCodes.count(code);
        };

        auto isOKResponse = [](auto code){
            return code == 200;
        };
        Request r(Uri(url), Headers{});

        try{
            auto res = NetworkManager::get(r);

            bool isOk = isOKResponse(res->statusCode());
            bool isRedir = !isOk &&  isRedirResponse(res->statusCode());

            if (!(isOk || isRedir)){
                //not supported
                Logger::log()<<"Cant read content:" << res->reasonPhrase();
                return nullptr;
            }

            if (isRedir){
                std::unordered_set<std::string> visitedUrls;
                while (isRedir){
                    visitedUrls.insert((const std::string&)r.uri());
                    const auto& newLocation = res->headers().get("Location");
                    if (newLocation.empty()){
                        throw std::runtime_error("Redirect Url is empty");
                    }
                    if (visitedUrls.count(newLocation)){
                        throw std::runtime_error("Loop detected");
                    }
                    r.setUri(Uri(newLocation));
                    res = NetworkManager::get(r);

                    isOk = isOKResponse(res->statusCode());
                    isRedir = !isOk &&  isRedirResponse(res->statusCode());

                    if (!(isOk || isRedir)){
                        //not supported
                        Logger::log()<<"Cant read content:" << res->reasonPhrase()<<'\n';
                        return nullptr;
                    }
                    if (isOk){
                        break;
                    }
                }
            }
            if (isOk){
                return res;
            }
        }
        catch (std::exception& e){
            Logger::log()<<e.what()<<'\n';
        }
        return nullptr;
    }
}


int main(int argc, char*argv[]) {
    std::string targetUrl = ArgNums::URI_ARG < argc ? argv[ArgNums::URI_ARG]: sDefaultUri;
    std::string resultFilePath = ArgNums ::LOCAL_FILE_PATH < argc ? argv[ArgNums::LOCAL_FILE_PATH]: sDefaultFilePath;

    Dataqueue<Response::DataBuf> chunkqueue;

    auto res = sendRequest(targetUrl);
    if (!res){
        return 0;
    }
    auto stepCount = std::ceil(res->contentLength()/1000.);
    ProgressMixin<FileDataWriter> writer(stepCount, resultFilePath);


    auto loaderThread = std::thread([&res, &chunkqueue](){

        while (1){
            try {
                Response::DataBuf buf;
                auto cur =res->readContent(buf);
                if (!cur){
                    break;
                }
                chunkqueue.push(std::move(buf));
            }
            catch (std::exception& e){
                (void)e;
                break;
            }
        }
        chunkqueue.setDone();
    });


    while (!chunkqueue.isDone()){
        Response::DataBuf chunk;
        if (!chunkqueue.pop(chunk)){
            continue;
        }
        writer.write((char*)chunk.data(), chunk.size());
    }

    loaderThread.join();
    return 0;
}