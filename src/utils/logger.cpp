#include "utils/logger.h"
#include <iostream>


using namespace my_wget_app::utils;

namespace {
    struct CLOGLogger : ILoger{
        virtual StreamType& getStream() const override{
            return std::clog;
        }
    };
}


Logger::Logger()
:m_pimpl(std::make_shared<CLOGLogger>())
{
}

ILoger& Logger::log(){
    static Logger slog;
    return *slog.m_pimpl;
}
