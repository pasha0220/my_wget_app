#include "progressbar.h"

#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>

using namespace my_wget_app::utils;

Progressbar::Progressbar(int stepCount)
: mStepCount(stepCount)
, mDonePercent(0)
, mStepWeight(1.f/stepCount)
{
    print();
}

void Progressbar::makeStep() {
    mDonePercent+=mStepWeight;
    print();
}

void Progressbar::print() const {
    int percent = std::min(10, (int)std::ceil(mDonePercent*10));
    std::cout<<"\r["<<std::string(percent, '*')<<std::string(10 - percent, '-')<<"]"<<std::flush;
}
