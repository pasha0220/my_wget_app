#include "file_data_writer.h"

using namespace my_wget_app::utils;

FileDataWriter::FileDataWriter(const std::string &path)
: mFstream(path, std::ios::out)
{
}

int FileDataWriter::write(const char *data, int size) {
    if (!mFstream){
        return 0;
    }
    mFstream.write(data, size);
    return size;
}

int FileDataWriter::write(const std::string &data) {
    if (!mFstream){
        return 0;
    }
    mFstream<<data;
    return data.size();
}
