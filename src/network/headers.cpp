#include "network/headers.h"

using namespace my_wget_app;

namespace {
    static const Headers::HeaderValue sEmptyHeader{};
}

Headers::Headers() : m_headers{}{}


Headers::HeaderIterator Headers::begin() {
    return m_headers.begin();
}

Headers::ConstHeaderIterator Headers::begin() const{
    return m_headers.cbegin();
}

Headers::HeaderIterator Headers::end() {
    return m_headers.end();
}

Headers::ConstHeaderIterator Headers::end() const {
    return m_headers.cend();
}


void Headers::add(const Headers::HeaderName &name, const Headers::HeaderValue &value) {
    m_headers[name] = value;
}

const Headers::HeaderValue &Headers::get(const Headers::HeaderName &name) const {
    auto pos = m_headers.find(name);
    if (pos == m_headers.end()){
        return sEmptyHeader;
    }
    return pos->second;
}
