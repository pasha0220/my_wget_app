#include "network/request.h"

using namespace my_wget_app;

Request:: Request(const Uri& uri, const Headers& header /*, cookies*/ )
: m_uri(uri), m_headers(header)
{
    m_headers.add("User-Agent", "my_wget_app");
    m_headers.add("Accept", "*/*");
    m_headers.add("Accept-Encoding", "identity");
    m_headers.add("Connection", "Keep-Alive");
}

Request::operator bool() const{
    return (bool)m_uri;
}

const Uri& Request::uri() const{
    return m_uri;
}

void Request::setUri(const Uri& uri){
    m_uri = uri;
}

const Headers& Request::headers() const{
    return m_headers;
}

Headers& Request::headers() {
    return m_headers;
}