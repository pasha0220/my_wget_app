#include "network/network_manager.h"
#include "network/request.h"
#include "network/socket_response.h"
#include "network/socket_data_writer.h"

#include <sys/socket.h> /* socket, connect */
#include <netinet/in.h> /* struct sockaddr_in, struct sockaddr */
#include <netdb.h> /* struct hostent, gethostbyname */

#include <string>
#include <cstring>

using namespace my_wget_app;

namespace {
    static int get_socket_descr(const my_wget_app::Request& request){
        if (!request){
            return -1;
        }
        auto sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (sockfd < 0){
            throw std::runtime_error("Cant create socket");
        }

        auto server = gethostbyname(request.uri().hostname().data());
        if (!server){
            throw std::runtime_error("Host not found");
        }

        struct sockaddr_in serv_addr;
        std::memset(&serv_addr, 0, sizeof(serv_addr));
        serv_addr.sin_family = AF_INET;
        serv_addr.sin_port = htons(request.uri().port());
        std::memcpy(&serv_addr.sin_addr.s_addr,server->h_addr,server->h_length);
        
        if (connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0){
            throw std::runtime_error("Cant connect to server");
        }
        return sockfd;
    }

    template <typename... Args>
    std::string create_line(Args&& ...args){
        std::string line;
        (line.append(std::forward<Args>(args)),...);
        line.append("\r\n");
        return line;


    }

    
    void make_get_request(int socketfd, const my_wget_app::Request& request){
        SocketDataWriter writer{socketfd};
        writer.write(create_line("GET ", request.uri().path(), " HTTP/1.1"));
        writer.write(create_line("Host: ", request.uri().hostname()));

        for (const auto& header: request.headers()){
             writer.write(create_line(header.first, ":", header.second));
        }
        writer.write("\r\n\r\n");
    }


}

std::unique_ptr<my_wget_app::Response> NetworkManager::get(const Request& request){
    const auto sock = get_socket_descr(request);
    make_get_request(sock, request);
    return std::make_unique<SocketResponse>(sock);
}

std::unique_ptr<my_wget_app::Response> NetworkManager::post(const Request& request){
    const auto sock = get_socket_descr(request);
    make_get_request(sock, request);
    return std::make_unique<SocketResponse>(sock);
}