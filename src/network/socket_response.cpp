#include "network/socket_response.h"
#include <unistd.h>

#include <iostream>
#include <sstream>
#include <string>
#include <cstring>
#include <algorithm>

using namespace my_wget_app;

namespace {
    static const size_t sDataBufMaxSize = 1000;
}

// HTTP/Версия Код состояния Пояснение

SocketResponse::SocketResponse(SocketResponse::SocketDescription sock)
:m_sock(sock)
, mStatusCode()
, mDataToRead(0)
, mContentLength(0)
, mReasonPhrase()
, mResponseHeaders()
, mRawData()
{
    mRawData.reserve(sDataBufMaxSize);
    init();
}

SocketResponse::~SocketResponse(){
    if (m_sock){
        close(m_sock);
    }
}

int SocketResponse::statusCode() const {
    return mStatusCode;
}

std::string_view SocketResponse::reasonPhrase() const {
    return std::string_view(mReasonPhrase.data(), mReasonPhrase.length());
}

size_t SocketResponse::contentLength() const {
    return mContentLength;
}

const Headers &SocketResponse::headers() const {
    return mResponseHeaders;
}

void SocketResponse::init() {
    
    auto getView = [](const std::string& str){
        std::string_view view(str.data(), str.size());
        view.remove_prefix(std::min(view.find_first_not_of(" \r\n"), view.size()));
        auto pos = view.find_last_of(" \r\n");
        if (pos != view.npos){
            view.remove_suffix(view.size() - pos);
        }
        return view;
    };


    Response::DataBuf buf;
    readSome(buf, sDataBufMaxSize);
    int emptyHeaderLineCount = 0;
    auto sizeRead = 0;

    std::string line;
    std::stringstream headerStream((char*)buf.data());
    if (std::getline(headerStream, line)){
        auto view = getView(line);
        parseStatusLine(view);
        sizeRead+=line.size()+1;
    }
    else{
        throw std::runtime_error("Cant read response header");
    }

    while (!emptyHeaderLineCount && std::getline(headerStream, line)){
        auto view = getView(line);
        if (view.size()){
            parseHeaderLine(view);
        }
        else{
            ++emptyHeaderLineCount;
        }
        sizeRead+=line.size()+1;
    }

    if (!emptyHeaderLineCount){
        //TODO: reread headers
        throw std::logic_error("Header is bigger then 1000 bytes. Header upload is not implemented");
    }else {
        const auto& str = headers().get("Content-Length");
        mContentLength = str.empty()? 0: std::stoi(str);

        const auto dataToRead = buf.size() - sizeRead;
        mRawData.insert(mRawData.begin(), std::next(buf.begin(), sizeRead), buf.end());
        mDataToRead = contentLength() - mRawData.size();

        readSome(buf, std::min(dataToRead, mDataToRead));
        mDataToRead -= buf.size();
        mRawData.insert(mRawData.end(), buf.begin(), buf.end());
    }    
}

void SocketResponse::readSome(Response::DataBuf& buf, size_t bytesToRead) const {
    buf.clear();
    buf.resize(std::min(bytesToRead, sDataBufMaxSize));
    size_t bytesRead = read(m_sock, buf.data(), bytesToRead);
    if (bytesRead == size_t(-1)){
        throw std::runtime_error("Cant read from socket");
    }
    if (bytesRead < buf.size()){
        buf.resize(bytesRead);
    }
}

void SocketResponse::parseStatusLine(std::string_view str) {
    auto firstSpase = str.find(' ');
    auto secondSpase = str.find(' ', firstSpase+1);
    if (firstSpase == str.npos || secondSpase == str.npos){
        throw std::runtime_error("Invalid response header");
    }
    char* end;
    mStatusCode = std::strtol(str.data()+firstSpase, &end, 10);
    mReasonPhrase = str.substr(secondSpase);
}

void SocketResponse::parseHeaderLine(std::string_view str) {
    auto pos = str.find(':');
    if (pos == str.npos){
        throw std::runtime_error("Invalid response header");
    }
    auto val_start = std::find_if(str.begin()+pos+1, str.end(), [](auto ch){
        return !std::isspace(ch);
    });

    const std::string key(str.substr(0, pos));
    const std::string value(str.substr(std::distance(str.begin(), val_start)));
    mResponseHeaders.add(key, value);
}

int SocketResponse::readContent(DataBuf& dataChunk) {
    auto bytesRead = mRawData.size();
    dataChunk = std::move(mRawData);
    
    readSome(mRawData, std::min(mDataToRead, sDataBufMaxSize));
    mDataToRead -= mRawData.size();

    return bytesRead;
}
