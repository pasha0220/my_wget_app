#include "network/socket_data_writer.h"
#include <unistd.h>

#include <iostream>

using namespace my_wget_app;

SocketDataWriter::SocketDataWriter(int socket_descr) :mSocket(socket_descr)
{
}

int SocketDataWriter::write(const char* data, int size) {
    if (!mSocket){
        return -1;
    }
    return ::write(mSocket, data, size);
}

int SocketDataWriter::write(const std::string& data) {
    if (!mSocket){
        return -1;
    }
    return ::write(mSocket, data.c_str(), data.length());
}

