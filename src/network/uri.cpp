#include "network/uri.h"

#include <regex>
#include <iostream>

using namespace my_wget_app;

namespace {
    static const std::string sDefaultPath{"/"};

    // void parseStr(const std::string& str){
    //     // from https://github.com/aosp-mirror/platform_frameworks_base/blob/master/core/java/android/util/Patterns.java
    //     static const std::string PROTOCOL = "(?:http)://";
    //     static const std::string PORT_NUMBER = "\\:\\d{1,5}";
    //     static const std::string IP_ADDRESS_STRING =
    //             "((25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])\\.(25[0-5]|2[0-4]"
    //             "[0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]"
    //             "[0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}"
    //             "|[1-9][0-9]|[0-9]))";
    //     static const std::string TLD_CHAR = "a-zA-Z";
    //     static const std::string LABEL_CHAR = "a-zA-Z0-9";
    //     static const std::string IRI_LABEL =
    //             "[" + LABEL_CHAR + "](?:[" + LABEL_CHAR + "_\\-]{0,61}[" + LABEL_CHAR + "]){0,1}";

    //     static const std::string PUNYCODE_TLD = "xn\\-\\-[\\w\\-]{0,58}\\w";
    //     static const std::string TLD = "(" + PUNYCODE_TLD + "|" + "[" + TLD_CHAR + "]{2,63}" +")";
    //     static const std::string HOST_NAME = "(" + IRI_LABEL + "\\.)+" + TLD;

    //     static const std::string USER_INFO = "(?:[a-zA-Z0-9\\$\\-\\_\\.\\+\\!\\*\\'\\(\\)"
    //                                          "\\,\\;\\?\\&\\=]|(?:\\%[a-fA-F0-9]{2})){1,64}(?:\\:(?:[a-zA-Z0-9\\$\\-\\_"
    //                                          "\\.\\+\\!\\*\\'\\(\\)\\,\\;\\?\\&\\=]|(?:\\%[a-fA-F0-9]{2})){1,25})?\\@";

    //     static const std::string PATH_AND_QUERY = "[/\\?](?:(?:[" + LABEL_CHAR
    //                                               + ";/\\?:@&=#~"
    //                                               "\\-\\.\\+!\\*'\\(\\),_\\$])|(?:%[a-fA-F0-9]{2}))*";

    //     static const std::string WORD_BOUNDARY = "(?:\\b|$|^)";



    //     static const std::string DOMAIN_NAME_STR = "(" + HOST_NAME + "|" + IP_ADDRESS_STRING + ")";


    //     static const std::string WEB_URL = "("
    //                               "("
    //                               "(?:" + PROTOCOL + "(?:" + USER_INFO + ")?" + ")?"
    //                               + "(?:" + DOMAIN_NAME_STR + ")"
    //                               + "(?:" + PORT_NUMBER + ")?"
    //                               + ")"
    //                               + "(" + PATH_AND_QUERY + ")?"
    //                               + WORD_BOUNDARY
    //                               + ")";




    //     std::regex re(WEB_URL);
    //     std::smatch match;
    //     std::cout<<"check regex "<<str<<std::endl;
    //     std::cout<<WEB_URL<<std::endl;
    //     if (std::regex_search(str, match, re)){
    //         for (size_t i = 0; i < match.size(); ++i){
    //             std::cout<<match[i]<<std::endl;
    //         }
    //     }

    // }
}


Uri::Uri(const std::string& uri) :mUri(uri)
{
    auto protoEnd = uri.find("://");
    size_t hostStart = protoEnd == uri.npos ? 0 : protoEnd+3;
    size_t hostEnd = uri.find('/', hostStart);
    mHost = uri.substr(hostStart, hostEnd == uri.npos ? uri.npos : hostEnd-hostStart);
    mPath = hostEnd == uri.npos ? sDefaultPath : uri.substr(hostEnd);

    //parseStr(uri);
}

Uri::Uri(const std::string &host,const std::string &path)
: mUri(host+"/"+path)
, mPort()
, mHost(host)
, mPath(path)
{
    //parseStr(host);
}


Uri::Uri(const Uri& o): mUri(o.mUri), mPort(o.mPort), mHost(o.mHost), mPath(o.mPath)
{
}

Uri::Uri(Uri&& o)
: mUri(std::move(o.mUri))
, mPort(std::move(o.mPort))
, mHost(std::move(o.mHost))
, mPath(std::move(o.mPath))
{
}

Uri& Uri::operator=(const Uri& o)
{
    if (this != &o){
        mUri = o.mUri;
        mHost = o.mHost;
        mPath = o.mPath;
        mPort = o.mPort;
    }
    return *this;
}

Uri& Uri::operator=(Uri&& o){
    mUri = std::move(o.mUri);
    mHost = std::move(o.mHost);
    mPath = std::move(o.mPath);
    mPort = std::move(o.mPort);
    return *this;
}


int Uri::port() const{
    if (mPort){
        return *mPort;
    }
    enum DefaultPort{HTTP = 80};
    return DefaultPort::HTTP;
}

Uri::operator bool() const {
    return !mUri.empty();
}

std::string_view Uri::hostname() const {
    return std::string_view(mHost.c_str(), mHost.length());
}

std::string_view Uri::path() const {
    return std::string_view(mPath.c_str(), mPath.length());
}

Uri::operator const std::string &() const {
    return mUri;
}